# SQL Data Generator

SQL Data Generator or 'random_sql' as named in the Rust project takes a list of table definitions and generates a select number of statements for each table requested

## Installation

Download Cargo Project and unzip, then run
```bash
cargo run --release
```
To run already generated executable after compiling run
```bash
./random_sql
```
On the executable that's created in /target/release/ after running
```bash
cargo run --release or cargo build --release
```

## Usage
To add tables to generate data for
Input Command : add 100 [table name] ([table attributes])
Input 'Help' for list of commands and examples
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Project status
Will update a few more times before this project is marked complete
Need Added/Fixed:
Mod | Modify command does not work on Rust version
Alter statement generation
Delete statement generation
Whatever else I decide to do
