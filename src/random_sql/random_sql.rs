#![allow(dead_code)]
use fake::{
    faker::{chrono::raw::*, internet::en::*, name::raw::*},
    locales::*,
    Fake, Faker,
};
use shlex;

use rand::{seq::SliceRandom, thread_rng, Rng};

use std::{
    collections::HashMap,
    fs,
    fs::OpenOptions,
    io::{self, stdin, stdout, Write},
};

pub(crate) fn check_data_type(attr_type: &str) -> bool {
    /*
    Check That The DateType Of Each Attribute Of A Table Is A Valid PostgresSQL DataType
    List Containing All Possible DataTypes in PostgresSQL...Hopefully
    This List Also Contains Custom Defined Type For Ease Of Data Generation
    EMAIL Is A Custom Type For This Program, This Ensures A Proper Email Format Is Generated
    NAME Is A Custom Type For This Program, This Ensures A Proper Name Style Is Generated
    GROUP Is A Custom Type For This Program, Ensures A Proper Role Is Generated For Simulation
    PASSWORD Is Custom Type For This Program, Ensures A Password Of Standard Complexity Is Generated

    :param attr_type: A string that contains the data type of referenced_attr
    :return: True if attr_type is a valid PostgresSQL data type, False otherwise
    */

    let valid_types = [
        "BIGINT",
        "BIT",
        "BOOLEAN",
        "BOX",
        "BYTEA",
        "CHAR",
        "CIDR",
        "CIRCLE",
        "EMAIL",
        "DATE",
        "DOUBLE PRECISION",
        "ENUM",
        "FLOAT4",
        "FLOAT8",
        "GROUP",
        "INET",
        "INTEGER",
        "NAME",
        "INTERVAL",
        "JSON",
        "JSONB",
        "LINE",
        "LSEG",
        "MACADDR",
        "MONEY",
        "NUMERIC",
        "PASSWORD",
        "PATH",
        "PG_LSN",
        "POINT",
        "POLYGON",
        "REAL",
        "SMALLINT",
        "SERIAL",
        "TEXT",
        "TIME",
        "TIMESTAMP",
        "TSQUERY",
        "TSVECTOR",
        "TXID_SNAPSHOT",
        "UUID",
        "XML",
        "VARCHAR",
    ];

    // Make Sure Passed DataType Exists For PostgresSQL
    valid_types
        .iter()
        .any(|substring| attr_type.starts_with(substring))
}

pub(crate) fn check_key_definition(key_def: &str) -> bool {
    /*
    Checks If A Keyed Attribute Is Properly Defined.
    PK -> Primary Key
    AK -> Alternate Key (Used To Define Unique Attributes)
    FK -> Foreign Key
    PK/FK -> Primary Key Of A Table That Is Also The Foreign Key Of A Different Table
    AK/FK -> Alternate Key Of A Table That Is Also The Foreign Key Of A Different Table

    :param key_def: A string that contains the definition of a key (PK, AK, FK, PK/FK, AK/FK)
    :return: True if key_def is a valid key definition, False otherwise
    */

    // Array Of Valid Keys
    let valid_keys = ["PK", "AK", "FK", "PK/FK", "AK/FK"];
    // Returns True If key_def Is In valid_keys, False Otherwise
    valid_keys.contains(&key_def)
}

pub(crate) fn check_pair(
    generated_pair_vector: &Vec<String>,
    previous_pair_vector: &Vec<Vec<String>>,
    table_attributes_vector: &Vec<String>,
    uq_attr_history: &HashMap<String, Vec<String>>,
    count: usize,
) -> (bool, Vec<String>) {
    /*
    Recursively Called Function
    Constantly Generates A New Pair Of Composite Key Values Until A Unique Pair Is Generated
    All Data Used In Pair Generation Comes From uq_attr_history Which Stores All Data Generated For Any Unique Key Value
    :param generated_pair
    */
    let mut new_pair: Vec<String> = Vec::new();

    // First Time Running With Generated Pair
    let pair_changed = count > 0;

    if previous_pair_vector.contains(&generated_pair_vector) {
        // Duplicate Pair Found
        // Must Generate New Data For Generated Pair
        for temp_attr in table_attributes_vector.clone() {
            let temp_attr_list = temp_attr.trim().split(' ').collect::<Vec<&str>>();
            if temp_attr_list[0] == "PK/FK" {
                let temp_attribute_name = temp_attr_list[3]
                    .split('(')
                    .nth(1)
                    .unwrap()
                    .replace(")", "");

                if let Some(history) = uq_attr_history.get(&temp_attribute_name) {
                    if let Some(value) = history.choose(&mut thread_rng()) {
                        new_pair.push(value.to_string()); // Cast to &str
                    }
                }
            }
        }
        return check_pair(
            &new_pair,
            previous_pair_vector,
            table_attributes_vector,
            uq_attr_history,
            count + 1,
        );
    }

    new_pair.extend_from_slice(&generated_pair_vector);
    (pair_changed, new_pair)
}

pub(crate) fn create_insert_statement(
    table_name: &str,
    generated_data_dict: &HashMap<String, String>,
    table_attributes: &Vec<String>,
) -> String {
    /*
     * Generates and returns a properly formatted SQL INSERT statement.
     *
     * Parameters:
     * - `table_name` (string): The name of the table the INSERT statement will be used on.
     * - `generated_data_dict` (HashMap<string, string>): A dictionary containing the data to be inserted into the table.
     *   The keys should match the column names of the table, and the values should be the data to be inserted.
     *
     * Returns:
     * - A string containing the properly formatted SQL INSERT statement.
     */

    // Start building the INSERT statement
    let mut insert_string = format!("INSERT INTO {} VALUES (", table_name);

    // Iterate over the generated data and add it to the INSERT statement
    // useless_key has no purpose and is only there to aid in iteration.
    for (index, attribute) in table_attributes.iter().enumerate() {
        let attribute_name: Vec<&str> = attribute.trim().split(" ").collect();
        let mut data: String = "".to_string();

        match attribute_name.len() {
            2 => {
                data = generated_data_dict
                    .get(attribute_name[0])
                    .unwrap()
                    .to_string();
            }
            3 => {
                data = generated_data_dict
                    .get(attribute_name[1])
                    .unwrap()
                    .to_string();
            }
            4 => {
                data = generated_data_dict
                    .get(attribute_name[1])
                    .unwrap()
                    .to_string();
            }
            _ => {}
        }

        // If we're at the last piece of data, close the statement with a semicolon
        if index == generated_data_dict.len() - 1 {
            if data.parse::<f64>().is_ok() {
                insert_string += &format!("{});", data);
            } else if data == "NULL" {
                insert_string += "NULL);";
            } else {
                insert_string += &format!("\'{}\');", data);
            }
            // If we're not at the last piece of data, add a comma to the end of the statement
        } else {
            if data.parse::<f64>().is_ok() {
                insert_string += &format!("{}, ", data);
            } else if data == "NULL" {
                insert_string += "NULL, ";
            } else {
                insert_string += &format!("\'{}\', ", data);
            }
        }
    }
    insert_string
}

pub(crate) fn generate_table_statements(
    tables_list: &Vec<String>,
    table_keys: &HashMap<String, Vec<String>>,
    referenced_keys: &HashMap<String, Vec<HashMap<String, String>>>,
    iterations: u16,
    custom_path: Option<String>,
) {
    /*
    Creates Random Data For Each Attribute And Generates A Statement Once They All Have Data
    Does This For Each Table Based Off Number Of Statements Requested For The Table
    */

    // Stores Unique Values Of Every Key: Key is Unique Attribute : Values -> List of Generated Values
    // THIS STORES EVERY UQ ATTRIBUTE AND VALUE FROM THE START OF THE PROGRAM TO THE END
    let mut uq_attr_check: HashMap<String, Vec<String>> = HashMap::new();

    let mut uq_pairwise_check: HashMap<String, Vec<Vec<String>>> = HashMap::new();

    let mut statements_generated = 0; // Tracks Statements Created To Compare To Total Number Of Requested Statements

    print!(
        "Generating SQL Inserts: {}/{} Created",
        statements_generated, iterations
    );
    stdout().flush().unwrap();
    /*
    Iterates Through Each Table Based Off Order Of Entry
    */
    for sql_table in tables_list.into_iter() {
        let sql_table: Vec<&str> = sql_table.split(" ").collect();

        let num_statements = sql_table[0].parse::<i32>().unwrap(); // Keeps Track of Number of Statements to Generate
        let table_name = String::from(sql_table[1]);
        let table_attributes: String = sql_table[2..]
            .join(" ")
            .trim_matches(|c| c == '(' || c == ')')
            .to_string();
        let table_attributes: Vec<String> = table_attributes
            .clone()
            .split(",")
            .map(|s| s.to_owned())
            .collect();

        //Used To Pass In Create Statement Functions To Keep Order Since Using Hashmap
        let table_attributes_passer: Vec<String> = table_attributes.clone();

        let mut primary_keys: Vec<String> = Vec::new();

        let mut pairwise_table = false;

        let check_references = referenced_keys.get(&table_name);

        // Check For Composite Key Table
        if let Some(_check_references) = check_references {
            for key_attribute in table_attributes.clone() {
                if key_attribute.trim()[..2] == "PK".to_string() {
                    primary_keys.push(String::from(key_attribute.to_string()));
                }
            }
            if primary_keys.len() > 1 {
                pairwise_table = true;
            }
        }

        for _ in 0..num_statements.clone() {
            // Stores Generated Values In Order That Attributes Were Entered For Table With Attribute Name As Key
            // Only Stores Values For The Current Statement, Resets Every Generation
            let mut statement_data_dict: HashMap<String, String> = HashMap::new();

            // Keep Track Of Previous Attribute [Used For Tables That Reference The Same Table(Attribute) More Than Once
            // Example (PK/FK userID1 INTEGER profile(userID), PK/FK userID2 INTEGER profile(userID)
            let mut referenced_attribute_dictionary: HashMap<String, Vec<String>> = HashMap::new();

            // Used In Pairwise (Composite) Key Scenarios
            let mut pair_list: Vec<String> = Vec::new();

            // Iterate Through Attributes Of Table Generating Data One-by-One
            for attr in table_attributes.clone() {
                let pairwise_attribute =
                    if pairwise_table && primary_keys.contains(&attr.to_string()) {
                        true
                    } else {
                        false
                    };

                let attr_definitions: Vec<&str> = attr.trim().split(" ").collect();

                match attr_definitions.clone().len() {
                    2 => {
                        /*
                        In This Case, Attribute Is Of Form
                        [Attribute Name][Attribute Type]
                        1. Check For Certain Types (To Get Size Of Variable) i.e Type = VARCHAR(100); Get Size 100
                        2. Generate Random Value For Attribute
                        3. In This Case, No Key Definition Is Provided So Values Don't Need To Be Unique
                        4. Add Attribute And Generated Data to generated_data_dict
                        */

                        let attr_name = attr_definitions[0];
                        let attr_type = attr_definitions[1];

                        let optional_variable_size: Option<u16> = set_variable_size(attr_type);

                        let generated_data: String = generate_random_data(
                            attr_type,
                            &statement_data_dict,
                            optional_variable_size,
                        );

                        statement_data_dict.insert(attr_name.to_owned(), generated_data);
                    }
                    3 => {
                        /*
                        In This Case, Attribute Is Of Form
                        [Unique Key Only][Attribute Name][Attribute Type]
                        1. Check For Certain Types (To Get Size Of Variable) i.e Type = VARCHAR(100); Get Size 100
                        2. Generate Random Value For Attribute
                        3. Assert Value Is Unique In uq_attr_check
                        4. If Unique, Add To uq_attr_check [See Definition Above] Else Generate New Value
                        5. Add Attribute And Generated Data to generated_data_dict
                        */

                        let attr_name = attr_definitions[1];
                        let attr_type = attr_definitions[2];

                        let optional_variable_size: Option<u16> = set_variable_size(attr_type);

                        // Randomly Generates Data For The Given Attribute
                        let mut randomized_data = generate_random_data(
                            &attr_definitions[2].clone(),
                            &statement_data_dict.clone(),
                            optional_variable_size.clone(),
                        );

                        if uq_attr_check.contains_key(&attr_definitions[1].to_string()) {
                            // Check if the key exists in the Key Dictionary
                            // Key Exists Check For Duplicate Values
                            let check_random_data = randomized_data.clone();
                            while uq_attr_check[&attr_definitions[1].to_string()]
                                .contains(&check_random_data)
                            {
                                // Runs Until randomized_data is Unique
                                randomized_data = generate_random_data(
                                    attr_definitions[2],
                                    &statement_data_dict,
                                    optional_variable_size,
                                );
                            }
                        }

                        // randomized_data Is Unique, Append to the existing list
                        uq_attr_check
                            .entry(attr_definitions[1].to_string())
                            .or_insert(Vec::new())
                            .push(randomized_data.to_string());

                        // Appends NonDuplicate Data To statement_data_dict With Attribute Name As Key
                        statement_data_dict.insert(attr_name.to_owned(), randomized_data.clone());

                        // If A Pairwise Unique Key
                        if pairwise_attribute {
                            pair_list.push(randomized_data);
                        }
                    }
                    4 => {
                        // Variables To Confirm Reference Exists
                        // Example profile(userID)
                        // Variable temp_table_reference = profile
                        // Variable temp_attribute_name = userID
                        let temp_table_reference =
                            &attr_definitions[3].split("(").next().unwrap().to_string();
                        let temp_attribute_name = &attr_definitions[3]
                            .split("(")
                            .nth(1)
                            .unwrap()
                            .replace(")", "");

                        if get_referenced_attribute(
                            table_keys.get(temp_table_reference).unwrap(),
                            &temp_attribute_name,
                        )
                        .is_none()
                        {
                            // Reference Not Found This Is A Program Error
                            println!("Invalid Foreign Key...Unable To Find Reference");
                            println!(
                                "This Error Occurred In Data Generation After Reference Checking"
                            );
                            println!("This Could Be A Program Error...Shutting Down, Please Debug Using Same Inputs");
                            std::process::exit(0);
                        }

                        loop {
                            let mut same_data_flag = false;

                            // Reference Does Exist And Is A Valid Type
                            // Get Random Reference
                            // Selects A Random Value From The List Of Generated Unique Values
                            let randomized_data = uq_attr_check
                                .get(temp_attribute_name)
                                .unwrap()
                                .choose(&mut thread_rng())
                                .unwrap()
                                .to_string();

                            // Check If Referenced Table Is Already In Reference Dictionary
                            // If So Then A Previous Attribute Already Referenced
                            // In Which Case Under Most Situations Make Sure The Two Values Are Not The Same
                            if referenced_attribute_dictionary
                                .contains_key(&attr_definitions[3].to_string())
                            {
                                // Checks Attribute Name: If Name Is In referenced_attribute_dictionary
                                // Then A Value For The Attribute Was Already Generated This Iteration
                                // This Is A Program Error
                                if referenced_attribute_dictionary
                                    .get(&attr_definitions[3].to_string())
                                    .unwrap()
                                    .contains(&&attr_definitions[1].to_string())
                                {
                                    println!("Data Being Generated For Attribute Twice");
                                    println!("Possible Program Error Detected");
                                    println!("Exiting Program");
                                    std::process::exit(0);
                                } else {
                                    let reference_list = referenced_attribute_dictionary
                                        .get(&attr_definitions[3].to_string())
                                        .unwrap();
                                    for temp_referenced_attribute in reference_list.clone() {
                                        if statement_data_dict
                                            .get(&temp_referenced_attribute.to_string())
                                            == Some(&&randomized_data)
                                        {
                                            // Both Attributes Referencing Same Table Have Same Values.
                                            // Assign New Value To Current Attribute
                                            same_data_flag = true;
                                            break;
                                        }
                                    }
                                    if same_data_flag {
                                        continue;
                                    }
                                }
                            }
                            if !pairwise_attribute {
                                // Check If Foreign Key Is Also A Unique Key Only Matters When Not A Paired Key
                                if attr_definitions[0].clone().starts_with("PK")
                                    || attr_definitions[0].clone().starts_with("AK")
                                {
                                    if uq_attr_check.contains_key(&attr_definitions[1].to_string())
                                        && uq_attr_check[&attr_definitions[1].to_string()]
                                            .contains(&&randomized_data)
                                    {
                                        continue;
                                    }
                                    // Not In UQ Dictionary So Add
                                    uq_attr_check
                                        .entry(attr_definitions[1].to_string())
                                        .or_insert(Vec::new())
                                        .push(randomized_data.to_string());
                                }
                            }

                            // Create List Of Referenced Attribute If Not Already Made
                            // If Already Made, It Just Appends
                            referenced_attribute_dictionary
                                .entry(attr_definitions[3].to_owned())
                                .or_insert(Vec::new())
                                .push(attr_definitions[1].to_owned());

                            if pairwise_attribute {
                                pair_list.push(randomized_data.clone());
                            }

                            // Appends NonDuplicate Data To statement_data_dict With Attribute Name As Key
                            statement_data_dict
                                .insert(attr_definitions[1].to_owned(), randomized_data.to_owned());
                            break;
                        }
                    }
                    _ => {
                        println!("Program Error In Generating Data");
                    }
                }
            }
            /*
            Check If uq_pairwise_check Contains Generated Composite Key
            Yes: Generate New Pairs Recursively Through check_pair Until Unique Value Is Generated
            No: Add To Dictionary And Continue
            */
            if pairwise_table {
                if !uq_pairwise_check.contains_key(&table_name)
                    || !uq_pairwise_check
                        .get(&table_name)
                        .unwrap()
                        .contains(&pair_list)
                {
                    uq_pairwise_check
                        .entry(table_name.to_string())
                        .or_insert(Vec::new())
                        .push(pair_list.clone());
                } else {
                    let (pair_changed, new_pair_list) = check_pair(
                        &pair_list,
                        uq_pairwise_check.get(&table_name).unwrap(),
                        &table_attributes,
                        &uq_attr_check,
                        0,
                    );
                    if pair_changed {
                        let mut counter = 0;
                        for rewrite_attribute in table_attributes.clone() {
                            let temp_attr_definitions: Vec<&str> =
                                rewrite_attribute.trim().split(' ').collect();
                            if temp_attr_definitions[0] == "PK/FK"
                                || temp_attr_definitions[0] == "AK/FK"
                            {
                                if counter >= pair_list.len() {
                                    println!(
                                        "Error There Are More Attributes Than Pairs Generated"
                                    );
                                    println!("Error Somewhere [This Was Never Supposed To Run]");
                                } else {
                                    statement_data_dict.insert(
                                        temp_attr_definitions[1].to_string(),
                                        pair_list[counter].clone(),
                                    );
                                    counter += 1;
                                }
                            }
                        }
                        uq_pairwise_check
                            .entry(table_name.to_string())
                            .or_insert(Vec::new())
                            .push(new_pair_list);
                    }
                }
            }

            /*
            Reiterate Through Generated Pairs And Check For Duplicates
            */
            if pairwise_table {
                for (check_key, pairs) in uq_pairwise_check.iter() {
                    let counts = pairs
                        .iter()
                        .map(|x| x.clone().into_iter().collect::<Vec<_>>())
                        .collect::<Vec<_>>()
                        .into_iter()
                        .collect::<std::collections::HashSet<_>>();
                    let mut duplicates = vec![];
                    for count in counts {
                        let found_pairs = pairs
                            .iter()
                            .filter(|x| count == x.iter().cloned().collect::<Vec<_>>())
                            .cloned()
                            .collect::<Vec<_>>();
                        if found_pairs.len() > 1 {
                            duplicates.push(found_pairs);
                        }
                    }
                    if !duplicates.is_empty() {
                        println!(
                            "Final check found duplicate pairs in {}: {:?}",
                            check_key, duplicates
                        );
                        println!("System Error Shutting Down Program");
                        std::process::exit(0);
                    }
                }
            }

            match write_to_file(
                create_insert_statement(
                    &table_name,
                    &statement_data_dict,
                    &table_attributes_passer,
                ),
                custom_path.clone(),
            ) {
                Ok(_) => {}
                Err(_) => {
                    panic!(
                        "Failed Writing Generating Statement For Table {} To File",
                        table_name
                    )
                }
            }
            statements_generated += 1;
            print!(
                "\rGenerating SQL Inserts: {}/{} Created",
                statements_generated, iterations
            );
            stdout().flush().unwrap();
        }
    }
}

pub(crate) fn generate_random_data(
    attribute_type: &str,
    generated_data_dict: &HashMap<String, String>,
    string_size: Option<u16>,
) -> String {
    /*
    Generates random data based on the provided attribute type.
    :param attribute_type: The type of attribute to generate random data for.
    :param generated_data_dict: A dictionary containing generated data that can be used in the function.
    :param string_size: An optional parameter that specifies the size of the generated string.
    :return: A randomly generated value based on the provided attribute type.
    */
    match attribute_type {
        "EMAIL" => {
            let domains = vec![
                "@Outlook.com",
                "@gmail.com",
                "@pitt.edu",
                "@yahoo.com",
                "@proton.mail",
                "@pm.me",
                "@paranoid.email",
            ];
            let username = match (
                generated_data_dict.get(&"name".to_string()),
                generated_data_dict.get(&"full name".to_string()),
                generated_data_dict.get(&"full_name".to_string()),
            ) {
                (Some(name), _, _) => name.replace(" ", ""),
                (_, Some(full_name), _) => full_name.replace(" ", ""),
                (_, _, Some(full_name_underscore)) => full_name_underscore.replace("_", ""),
                _ => {
                    println!("No Name Found For User. Generating Random Email");
                    Faker.fake::<String>().replace(" ", "")
                }
            };
            format!("{}{}", username, domains.choose(&mut thread_rng()).unwrap())
        }
        "DATE" => Date(EN).fake(),
        "TIMESTAMP" => DateTime(EN).fake(),
        "INTEGER" => Faker.fake::<u32>().to_string(),
        "FLOAT" => Faker.fake::<f32>().to_string(),
        "BOOLEAN" => Faker.fake::<bool>().to_string(),
        "GROUP" => ["Member", "Admin"]
            .choose(&mut thread_rng())
            .unwrap()
            .to_string(),
        password_type if password_type.starts_with("PASSWORD") => Password(std::ops::Range {
            start: (8),
            end: (string_size.unwrap_or(20) as usize),
        })
        .fake(),
        varchar_type if varchar_type.starts_with("VARCHAR") || varchar_type.starts_with("CHAR") => {
            let string_size = string_size.unwrap_or_else(|| thread_rng().gen_range(8..30));
            Faker
                .fake::<String>()
                .chars()
                .take(string_size as usize)
                .collect()
        }
        name_type if name_type.starts_with("NAME") => {
            let name: String = Name(EN).fake();
            name.replace("'", "''")
        }
        _ => {
            panic!("Unknown Type In Data Generation! {}", attribute_type);
        }
    }
}

pub(crate) fn get_referenced_attribute(attr_list: &Vec<String>, referenced_attr: &str) -> Option<String> {
    /*
    Returns the Table Attribute Being Referenced By Another Table
    Returns None If Reference Doesn't Exist

    :param attr_list: A vector of all keyed attributes from a table
    :param referenced_attr: A string containing the name of the referenced attribute
    :return: An optional string representation of the referenced attribute if it exists, None otherwise
    */

    if attr_list.is_empty() {
        return None;
    }

    for attr_element in attr_list {
        if attr_element.split_whitespace().nth(1) == Some(referenced_attr) {
            return Some(attr_element.to_string());
        }
    }

    None
}

// Main Function
pub(crate) fn main() {
    /*
    Main Function For randomSQL
    Handles Input Parsing, Reference Matching, and Establishing Unique Keyed Attributes
    Includes All Input Commands Such As 'Generate', 'Exit', 'Add', 'Modify', 'Del', 'Show'
    */

    let mut custom_path: Option<String> = None;
    let mut tuples_to_generate: Vec<String> = Vec::new();
    let mut key_dictionary: HashMap<String, Vec<String>> = HashMap::new();
    let mut reference_dictionary: HashMap<String, Vec<HashMap<String, String>>> = HashMap::new(); // Not Used For Any Real Data Generation
    let mut total_iterations: u16 = 0;

    display_help(false);

    loop {
        print!("[*] Manage Tables -> ");
        let _ = stdout().flush();
        let mut sql_command = String::new();
        stdin()
            .read_line(&mut sql_command)
            .expect("Failed to read line");

        // Input Handler
        // Using a match statement to handle multiple cases
        match sql_command.trim().to_lowercase().as_str() {
            // Program has at least 1 table to generate inserts for and will run
            "generate" if !tuples_to_generate.is_empty() => {
                let mut final_path = custom_path.clone();
                generate_table_statements(
                    &tuples_to_generate,
                    &key_dictionary,
                    &reference_dictionary,
                    total_iterations,
                    custom_path,
                );
                if final_path.is_none() {
                    let user_folder = dirs::home_dir().unwrap();
                    final_path = Some(
                        user_folder
                            .join("Documents/sample-data.sql")
                            .to_string_lossy()
                            .to_string(),
                    );
                }
                let (final_directory, final_file) = final_path
                    .as_ref()
                    .map_or((".", ""), |p| p.rsplit_once('/').unwrap_or((".", "")));
                println!(
                    "\nFinished.. {} Statements Created And Saved To File..
                    Look In {} For An SQL Script Called {}",
                    total_iterations, final_directory, final_file
                );
                break;
            }
            // Program doesn't have any tables to generate inserts for and cannot run
            "generate" if tuples_to_generate.is_empty() => {
                println!("No INSERT Statements Added! Please Provide At Least 1 Table To Generate Statements For!");
            }
            // Clear SQL Terminal
            "clear" => {
                print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
            }
            // Display help message
            "help" => {
                display_help(true);
            }
            // Exit program
            "exit" | "quit" => {
                if !tuples_to_generate.is_empty() {
                    println!("All Table Data Will Be Lost! Be Sure To Generate Inserts!");
                    print!("Quit Anyway? [Yes | No] -> ");
                    let _ = stdout().flush();
                    let mut confirm = String::new();
                    stdin()
                        .read_line(&mut confirm)
                        .expect("Failed to read line");
                    if confirm.trim().to_lowercase() == "yes"
                        || confirm.trim().to_lowercase() == "y"
                    {
                        break;
                    }
                } else {
                    break;
                }
            }
            _ => {
                sql_command = sql_command.replace("\\", "\\\\");
                let command_tokens = shlex::split(sql_command.trim()).unwrap_or_default();
                let sql_command_list: Vec<&str> =
                    command_tokens.iter().map(|s| s.as_str()).collect();
                if sql_command_list.len() < 2 {
                    println!("No Argument Provided For Data Manipulation Commands");
                    println!("Type 'help' To See How To Use Commands");
                    continue;
                }

                match sql_command_list[0].to_lowercase().as_str() {
                    "path" => {
                        let path = sql_command_list[1].to_string();
                        let path_obj = std::path::Path::new(&path);
                        let filename = path_obj.file_name().unwrap().to_str().unwrap();
                        let directory = path_obj.parent().unwrap().to_str().unwrap();

                        if !std::path::Path::new(&directory).exists() {
                            println!("Path To File Unreachable");
                            continue;
                        } else if std::path::Path::new(&path).exists() {
                            println!("File {} Already Exists In {}", &filename, &directory);
                            let mut choice = String::new();
                            print!("Overwrite Existing File? [Yes | Y || No | N]: ");
                            stdout().flush().unwrap();
                            stdin().read_line(&mut choice).unwrap();
                            let choice = choice.to_lowercase();
                            let choice = choice.trim();

                            if choice == "yes" || choice == "y" {
                                fs::write(&path, "").unwrap();
                                println!("File {} in {} Overwritten", &filename, &path);
                            } else {
                                println!("File not overwritten");
                                println!("Please Enter A New Path or Use Default");
                                continue;
                            }
                        } else {
                            fs::write(&path, "").unwrap();
                            println!("File {} in {} Created", &filename, &directory);
                        }
                        custom_path = Some(path);
                    }
                    "add" => {
                        let value = sql_command_list[1].parse::<u16>();
                        match value {
                            Ok(iterations) => {
                                if iterations <= 0 {
                                    println!(
                                        "Invalid Iteration Count. Iterations Should Be At Least 1"
                                    );
                                } else {
                                    total_iterations += iterations;
                                }
                            }
                            Err(_) => {
                                println!("Error Casting Iteration Count For Table To Int");
                                println!("Please Input A Natural Number Greater Than Zero");
                                continue;
                            }
                        };

                        let attributes = sql_command_list[3..sql_command_list.len()].join(" ");
                        let attributes: String = attributes[1..attributes.len() - 1].to_string();
                        let attributes = attributes.split(",").collect::<Vec<&str>>();
                        for attribute in attributes {
                            let attribute_check =
                                attribute.trim().split(" ").collect::<Vec<&str>>();
                            match attribute_check.len() {
                                2 => {
                                    if !check_data_type(attribute_check[1].to_uppercase().as_str())
                                    {
                                        println!("Invalid DataType For PostgresSQL");
                                        break;
                                    }
                                }
                                3 => {
                                    if !check_key_definition(attribute_check[0]) {
                                        println!("Invalid Key Definition");
                                        break;
                                    }
                                    if !check_data_type(attribute_check[2].to_uppercase().as_str())
                                    {
                                        println!("Invalid DataType For PostgresSQL");
                                        break;
                                    }
                                    if attribute_check[0] == "PK" || attribute_check[0] == "AK" {
                                        key_dictionary
                                            .entry(sql_command_list[2].to_string())
                                            .or_insert(Vec::new())
                                            .push(attribute.to_string());
                                    }
                                }
                                4 => {
                                    if !check_key_definition(attribute_check[0]) {
                                        println!("Invalid Key Definition");
                                        break;
                                    }
                                    if !check_data_type(attribute_check[2].to_uppercase().as_str())
                                    {
                                        println!("Invalid DataType For PostgresSQL");
                                        break;
                                    }
                                    let table_reference =
                                        attribute_check[3].split("(").collect::<Vec<&str>>()[0];
                                    let attribute_name =
                                        attribute_check[3].split("(").collect::<Vec<&str>>()[1]
                                            .replace(")", "");
                                    let referenced_attribute = get_referenced_attribute(
                                        key_dictionary
                                            .get(&table_reference.to_string())
                                            .unwrap_or(&&vec![]),
                                        &attribute_name,
                                    );
                                    match referenced_attribute {
                                        Some(_) => {
                                            if attribute_check[0].starts_with("PK")
                                                || attribute_check[0].starts_with("AK")
                                            {
                                                key_dictionary
                                                    .entry(sql_command_list[2].to_string())
                                                    .or_insert(Vec::new())
                                                    .push(attribute.to_string());
                                            }
                                            reference_dictionary
                                                .entry(sql_command_list[2].to_string())
                                                .or_insert(Vec::new())
                                                .push(HashMap::from([(
                                                    table_reference.to_string(),
                                                    attribute_name.to_string(),
                                                )]));
                                        }
                                        None => {
                                            println!(
                                                "Invalid Foreign Key...Unable To Find Reference"
                                            );
                                            break;
                                        }
                                    }
                                }
                                _ => {
                                    println!("Error In Attributes For The Table: {}...Attribute: {} Is Not Formatted Properly", sql_command_list[2], attribute);
                                    break;
                                }
                            }
                        }
                        let add_table_string =
                            sql_command_list[1..sql_command_list.len()].join(" ");
                        tuples_to_generate.push(add_table_string);
                        println!(
                            "Table {} Added. Type 'Generate' To Create Statements",
                            sql_command_list[2]
                        );
                    }
                    "remove" | "rm" => {
                        let table_to_delete = tuples_to_generate
                            .iter()
                            .find(|elem| {
                                elem.split_whitespace().nth(1) == Some(sql_command_list[1])
                            })
                            .cloned();
                        if let Some(table) = table_to_delete {
                            tuples_to_generate.retain(|elem| elem != &table);
                        } else {
                            println!("Table Specified Not In List");
                            println!("Possible Error In Name Entry? Run 'show inserts' To Get Correct Name");
                        }
                    }
                    "modify" | "mod" => {
                        // Commands Are In The Format Of
                        // Modify [tableName] [numInserts | tableName | referenced_attr] [newValue]
                        if sql_command_list.len() == 4 {
                            let table_specifier = sql_command_list[1].to_lowercase();
                            let modification = sql_command_list[2].to_lowercase();
                            let new_value = sql_command_list[3].to_string();
                            let mut tuple_iterator = tuples_to_generate.clone();
                            for (index, table) in tuple_iterator.iter_mut().enumerate() {
                                let cloned_table = table.clone();
                                let table_parts: Vec<&str> =
                                    cloned_table.split_whitespace().collect();
                                if table_specifier == table_parts[1].to_lowercase() {
                                    let attribute_string = table_parts[2..].join(" ");
                                    println!(
                                        "Old Table -> {}: Associated Tuple -> {}",
                                        table_parts[1], table
                                    );
                                    match modification.as_str() {
                                        "numinserts" | "numstatements" | "numiter" => {
                                            let table_piece = table_parts[1..].join(" ");
                                            let insert_value = new_value.clone();
                                            let print_statement =
                                                format!("{} {}", insert_value, table_piece).clone();
                                            tuples_to_generate[index] = print_statement;
                                        }
                                        "tablename" | "table" | "tname" | "name" => {
                                            let insert_value = new_value.to_lowercase();
                                            let first_table_piece = table_parts[0];
                                            let last_table_piece = table_parts[2..].join(" ");
                                            let print_statement = format!(
                                                "{} {} {}",
                                                first_table_piece, insert_value, last_table_piece
                                            );
                                            tuples_to_generate[index] = print_statement;
                                        }
                                        _ => {
                                            if attribute_string.contains(&modification) {
                                                let mut attribute_string_vec: Vec<&str> =
                                                    attribute_string
                                                        .trim_matches(|c| c == '(' || c == ')')
                                                        .split(',')
                                                        .map(|s| s.trim())
                                                        .collect();
                                                if let Some(attr_index) = attribute_string_vec
                                                    .iter()
                                                    .position(|&attr| attr.contains(&modification))
                                                {
                                                    let mut attribute = attribute_string_vec
                                                        [attr_index]
                                                        .to_string();
                                                    if attribute.starts_with(' ') {
                                                        attribute = format!(" {}", new_value);
                                                    } else {
                                                        attribute = new_value;
                                                    }
                                                    attribute_string_vec[attr_index] =
                                                        attribute.as_str();
                                                    let first_table_piece = table_parts[0];
                                                    let mid_table_piece = table_parts[1];
                                                    let print_statement = format!(
                                                        "{} {} {}",
                                                        first_table_piece,
                                                        mid_table_piece,
                                                        attribute_string_vec.join(", ")
                                                    );
                                                    tuples_to_generate[index] = print_statement;
                                                }
                                            }
                                        }
                                    }
                                    println!(
                                        "New Table -> {}: Associated Tuple -> {}",
                                        table_parts[1], table
                                    );
                                    break;
                                }
                            }
                        } else {
                            println!("Error In Modify Command...");
                            println!("Please Input Command Using The Following Format:");
                            println!("CMD -> Modify [tableName] [numInserts | tableName | referenced_attr] [newValue]");
                        }
                    }
                    "show" => {
                        // If len == 3 Then A Specific Table Has Been Given To Pull Data From
                        // Else
                        let specifier = if sql_command_list.len() == 3 {
                            Some(sql_command_list[2].to_lowercase())
                        } else {
                            None
                        };

                        match sql_command_list[1].to_lowercase().as_str() {
                            // Show Every Table The Program Will Generate Inserts For [Unless Given A Specific Table]
                            "inserts" => {
                                for table in &tuples_to_generate {
                                    if specifier.is_none()
                                        || *specifier.as_ref().unwrap()
                                            == table.split(' ').nth(1).unwrap().to_lowercase()
                                    {
                                        println!(
                                            "Table -> {}: Associated Tuple -> {}",
                                            table.split(' ').nth(1).unwrap(),
                                            table
                                        );
                                    }
                                }
                            }
                            // Show The Keys For Every Table [Unless Given A Specific Table]
                            "keys" => {
                                for (table, keys) in &key_dictionary {
                                    if specifier.is_none()
                                        || *specifier.as_ref().unwrap()
                                            == table.split(' ').nth(1).unwrap().to_lowercase()
                                    {
                                        println!("Keys For Table {} -> {:?}", table, keys);
                                    }
                                }
                            }
                            // Show's All Tables That Reference Another Table [Unless Given A Specific Table]
                            "references" | "refs" => {
                                for (ref_table, key) in &reference_dictionary {
                                    if specifier.is_none()
                                        || *specifier.as_ref().unwrap()
                                            == ref_table.split(' ').nth(1).unwrap().to_lowercase()
                                    {
                                        for value in key {
                                            for (r_key, r_value) in value {
                                                println!("Referencing Table -> {} References {} From Table {}", ref_table, r_value, r_key);
                                            }
                                        }
                                    }
                                }
                            }
                            // Show's All Custom Types Made Specifically For This Program
                            "types" => {
                                println!(
                                    "
                                        Custom Types Created For This Program
                                        NAME -> Name is a custom type used to replace typical string DataTypes in sql for
                                        when the user wishes to generate a realistic name, assigning a typical
                                        VARCHAR(length) to an attribute where you wish to be a name may result in a randomly
                                        generated string that does not represent a persons full name in real life

                                        EMAIL -> Email is another custom type used to replace string DataTypes in sql. This
                                        ensures that every value generated for said attribute will follow standard email
                                        format 'abc@abc.abc' and also excludes symbols that are commonly excluded in
                                        deployed email domain services (^&% etc)

                                        PASSWORD(N) -> Password is another custom type used to replace string DataTypes in sql.
                                        While it generates similar strings that would be generated should you just do a
                                        VARCHAR(length) variable in its place, PASSWORD types have the added step of 
                                        ensuring common password complexity constraints are enforced such as
                                        Password Must Include At Least: 
                                        [1 Capital Letter, 1 Lowercase, 1 Number, 1 Special Char]
                                        [N = Max Length Of Password And It Generates Length Between 8..N]
                                        [NOTE] IF YOU DO NOT ADD A LENGTH TO PASSWORD I.E PASSWORD(40)
                                        [THEN] THE PROGRAM WILL PANIC. THIS INCLUDES STANDARD VARCHAR(N) AND CHAR(N) TYPES

                                        GROUP -> Is a custom DataType that technically would replace String SQL types.
                                        Assigning this type to an attribute will assign a random role common in
                                        online groups. Such as 'Member', 'Admin', 'Founder'. User may add extra roles to
                                        the decision list if the group they are simulating inserts on contains additional
                                        roles
                                        "
                                    );
                            }
                            // Show's Examples Of Commands To Aid The User Optional [Specifier] Shows Only Specific Examples
                            // If Specifier Not Given. All Examples Are Shown
                            "examples" => {
                                // Show's Examples Of How To Add Tables
                                if specifier.is_none() || specifier == Some("add".to_owned()) {
                                    // Examples For Add
                                    println!("
                                        Add Example [Adding 'profile' Table To List]:
                                        [Add Example #1] -> add 100 profile (PK userID INTEGER, name NAME, AK email EMAIL, password PASSWORD(30), dateOfBirth DATE, lastLogin TIMESTAMP)
                                    ");
                                }
                                // Show's Examples Of How To Delete Tables
                                if specifier.is_none() || specifier == Some("del".to_owned()) {
                                    // Examples For Del
                                    println!(
                                        "
                                        Rm Example [Assuming 'profile' Table Is Defined]:
                                        [Rm Example #1] -> Rm profile
                                    "
                                    );
                                }
                                // Show's Examples Of How To Modify Tables
                                if specifier.is_none()
                                    || specifier == Some("modify".to_owned())
                                    || specifier == Some("mod".to_owned())
                                {
                                    // Examples For Modify
                                    println!(
                                        "
                                        Modify Example [Assuming 'profile' Table Is Defined From 'Add' Example]:
                                        [In This Case This Example Converts The AK 'email' Attribute To A PK]
                                        [These Examples Were Added Before EMAIL Was Made A Custom Type]
                                        [Therefore We Will Also Show How One Would Change The Type]
                                        [Note: For Email Attributes It Is Recommended To User EMAIL Instead of VARCHAR]
                                        [Currently The Email Attribute is 'AK email VARCHAR(50)']
                                        [Modify Example #1] -> 'Modify profile 'email' 'PK email EMAIL''
                                        [New Email Attribute Is 'PK email EMAIL' In Table 'profile']
                                        [In This Case This Example Changes Inserts To Generate From 100 to 300]
                                        [Modify Example #2] -> 'Modify profile numInserts 300'
                                        [In This Case This Example Changes Table Name 'profile' to 'account']
                                        [Modify Example #3] -> 'Modify profile tableName account'
                                        Note: The Use Of Single Quotes When Inputting Commands Is Not Needed
                                        And Is There To Aid Readability For User When Viewing Examples
                                        "
                                    );
                                }
                                // Explains Reference Formatting To Aid In Table Input
                                if specifier.is_none()
                                    || specifier == Some("references".to_owned())
                                    || specifier == Some("refs".to_owned())
                                {
                                    println!(
                                        "
                                        Attribute Format Explained:
                                        All Attributes In A Table Definition Must Be Of The Form:
                                        '[key definition][attribute name][attribute type][foreign table]'
                                        Where [key definition] And [foreign table] Can Be Null

                                        If [key definition] Is A Foreign Key [FK | PK/FK | AK/FK]:
                                            Then [foreign table] MUST be defined

                                        The Following Is An Example PK/FK Attribute
                                        Example References A Table Named 'profile' And Its Attribute 'userID'
                                        Attribute -> 'PK/FK userID1 INTEGER profile(userID)'
                                        The Example Would Assign As So:
                                            [key definition] = 'PK/FK'
                                                Is A Primary Key That References Primary Key 'userID' From Table 'profile'
                                            [attribute name] = 'userID1'
                                                Self Explanatory, It Is The Name Of The Attribute Being Defined
                                            [attribute type] = 'INTEGER' 
                                                Type Definitions On Each Attribute Must Match SQL DataTypes, Not Pythons
                                            [foreign table] = 'profile(userID)' Where:
                                                [*] 'profile' Is The Table Being Referenced
                                                [*] 'userID' Is The Referenced Attribute
                                        [*] For More Help With Attributes, Type 'show examples attributes'

                                        In Tables With More Than One PK, It Will Generate Data As A Composite Pair,
                                        Hence The AK Attribute May Be Needed, This Is Enforces The Unique Value Without
                                        Worrying About Checking The Keys As Pairs

                                        In Cases With Tables Having Multiple Attributes: 
                                        They Must Be Placed In () And Separated By Commas.
                                        'show examples add' Provides Examples
                                        "
                                    );
                                }
                                // Explains Attribute Formatting To Aid In Table Input
                                if specifier.is_none()
                                    || specifier == Some("attributes".to_owned())
                                    || specifier == Some("attr".to_owned())
                                {
                                    println!(
                                        "
                                            Basic Introductory To Attribute Types For SQL Tables
                                        ---------------------------------------------------------------------------
                                        [*] To See Custom Defined Types Specifically For This Program
                                        [*] Type 'show examples types'

                                        [Example 1]: 'age INTEGER'
                                        [key definition] = None
                                            [*] No Key Definition is Included
                                        [attribute name] = 'age'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'INTEGER'
                                            [*] Must Be SQL Data Types Not Rust Data Types

                                        [Example 2]: 'salary DECIMAL(10,2)'
                                        [key definition] = None
                                            [*] 'No Key Definition is Included
                                        [attribute name] = 'salary'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'DECIMAL(10,2)'
                                            [*] Must Be SQL Data Types Not Rust Data Types

                                        [Example 3]: 'name NAME'
                                        [key definition] = None
                                            [*] No Key Definition is Included
                                        [attribute name] = 'orderID'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'NAME'
                                            [*] Must Be SQL Data Types Not Rust Data Types
                                            [*] NAME Is A Custom Type Made Specifically For This Program
                                            [*] NAME Type Ensures A Realistic Full Name Is Generated
                                            [*] To See Custom Defined Types Specifically For This Program
                                            [*] Type 'show examples types'

                                        [Example 4]: 'PK userID INTEGER'
                                        [*] Is a Primary Key Attribute
                                        [key definition] = 'PK'
                                            [*] 'PK' stands for Primary Key
                                        [attribute name] = 'userID'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'INTEGER'
                                            [*] Must Be SQL Data Types, Not Rust Data Types

                                        [Example 5]: 'AK email EMAIL'
                                        [*] Is a Alternate Key Attribute
                                        [key definition] = 'AK'
                                            [*] 'AK' stands for Alternate Key
                                        [attribute name] = 'userID'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'EMAIL'
                                            [*] Must Be SQL Data Types, Not Rust Data Types
                                            [*] EMAIL Is A Custom Type Made Specifically For This Program
                                            [*] EMAIL Type Ensures A Realistic Email Is Generated
                                            [*] If A Name Attribute Is Present In The Same Table And Data Gets Generated For It Before Email
                                            [*] The Email Generated Will Be Representative Of The Name Generated As Well
                                            [*] Example If A Name 'Bob Johnson' Is Generated For A Tuple:
                                            [*] Then A Possible Email Generated Would Be BobJohnson@outlook.com
                                            [*] To See Custom Defined Types Specifically For This Program
                                            [*] Type 'show examples types'

                                        [Example 6]: 'FK customerID INTEGER customers(customerID)'
                                        [*] Is a Foreign Key Attribute Referencing the 'customers' Table's 'customerID' Attribute
                                        [key definition] = 'FK'
                                            [*] 'FK' stands for Foreign Key
                                        [attribute name] = 'customerID'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'INTEGER'
                                            [*] Must Be SQL Data Types, Not Rust Data Types

                                        [Example 7]: 'FK customerID INTEGER customers(customerID)'
                                        [*] Is a Foreign Key Attribute Referencing the 'customers' Table's 'customerID' Attribute
                                        [key definition] = 'FK'
                                            [*] 'FK' stands for Foreign Key
                                        [attribute name] = 'customerID'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'INTEGER'
                                            [*] Must Be SQL Data Types, Not Rust Data Types

                                        [Example 8]: 'AK/FK customerID INTEGER references customers(customerID)'
                                        [*] Is an Alternate Key and Foreign Key Attribute References the 'customers' Table's 'customerID' Attribute
                                        [key definition] = 'AK/FK'
                                            [*] 'AK/FK' stands for Alternate Key/Foreign Key
                                        [attribute name] = 'customerID'
                                            [*] Name of Attribute Being Defined
                                        [attribute type] = 'INTEGER'
                                            [*] Must be SQL Data Types, not Rust Data Types

                                        [Example 9]: 'PK/FK bookID INTEGER authors(bookID)'
                                        [*] Is a Primary Key and Foreign Key Attribute Referencing the 'authors' Table's 'bookID' Attribute
                                        [key definition] = 'PK/FK'
                                            [*] 'PK' stands for Primary Key
                                            [*] 'FK' stands for Foreign Key
                                            [*] 'PK/FK' stands for Primary Key with a Foreign Reference
                                        [attribute name] = 'bookID'
                                            [*] Name of Attribute being Defined
                                        [attribute type] = 'INTEGER'
                                            [*] Must Be SQL Data Types, Not Rust Data Types
                                        [foreign table] = 'authors(bookID)'
                                            [*] Where Again, 'authors' Is The Table Being Referenced and 'bookID' Is The Referenced Attribute From The Table

                                        [Example 10]: 'PK userID INTEGER, PK postID INTEGER
                                        [*] These Primary Key Attributes Create A Composite Key
                                        [key definition] = 'PK'
                                            [*] 'PK' stands for Primary Key
                                        [attribute name 1] = 'userID'
                                            [*] Name of the First Attribute Being Defined
                                        [attribute type 1] = 'INTEGER'
                                            [*] Must be SQL Data Types, Not Rust Data Types
                                        [attribute name 2] = 'postID'
                                            [*] Name of the second Attribute Being Defined
                                        [attribute type 2] = 'INTEGER'
                                            [*] Must be SQL Data Types, not Rust Data Types
                                        [*] Both 'userID' and 'postID' are Primary Keys
                                        [*] This Means Their Combination Will Be Treated As A Composite Key Pair When Generating Random Data For The Table. 
                                        [*] This Means That Any Two Rows On The Table Cannot Have The Same Combination of 'userID' and 'postID'.

                                        [Example 11]: 'PK/FK bookID INTEGER authors(bookID), PK/FK publisherID INTEGER publishers(publisherID)'
                                        [*] These Primary Key Attributes Create A Composite Key Where Each Attribute Also References An Exterior Table
                                        [key definition] = 'PK/FK'
                                            [*] 'PK' stands for Primary Key
                                            [*] 'FK' stands for Foreign Key
                                            [*] 'PK/FK' stands for Primary Key with a Foreign Reference
                                        [attribute name 1] = 'bookID'
                                            [*] Name of the First Attribute being Defined
                                        [attribute type 1] = 'INTEGER'
                                            [*] Must Be SQL Data Types, Not Rust Data Types
                                        [attribute name 2] = 'publisherID'
                                            [*] Name of the Second Attribute being Defined
                                        [attribute type 2] = 'INTEGER'
                                            [*] Must Be SQL Data Types, Not Rust Data Types
                                        [foreign table] = 'authors(bookID), publishers(publisherID)'
                                            [*] Where Again, 'authors' Is The Table Being Referenced and 'bookID' Is The Referenced Attribute From The Table
                                            [*] As Well As, 'publisher' Is The Table Being Referenced and 'publisherID' Is The Referenced Attribute From The Table
                                            [*] Both 'userID' and 'postID' are Primary Keys
                                            [*] This Means Their Combination Will Be Treated As A Composite Key Pair When Generating Random Data For The Table. 
                                            [*] This Means That Any Two Rows On The Table Cannot Have The Same Combination of 'userID' and 'postID'.

                                        [Example 12]: 'PK userID INTEGER, AK email EMAIL'
                                        [key definition 1] = 'PK'
                                            [*] 'PK' stands for Primary Key
                                        [attribute name 1] = 'userID'
                                            [*] Name of Attribute Being Defined
                                        [attribute type 1] = 'INTEGER'
                                            [*] Must be SQL Data Types, not Rust Data Types
                                        [key definition 2] = 'AK'
                                            [*] 'AK' stands for Alternate Key
                                        [attribute name 2] = 'email'
                                            [*] Name of Attribute Being Defined
                                        [attribute type 2] = 'EMAIL'
                                            [*] Must be SQL Data Types, not Rust Data Types
                                            [*] EMAIL Is A Custom Type Made Specifically For This Program
                                            [*] EMAIL Type Ensures A Realistic Email Is Generated
                                            [*] To See Custom Defined Types Specifically For This Program
                                            [*] Type 'show examples types'
                                            [*] In This table, The 'userID' Attribute Is A Primary Key And The 'email' Attribute Is An Alternate Key.
                                            [*] While Having Multiple Primary Keys In A Table Leads To A Composite Key Pair.
                                            [*] Having A Primary Key And An Alternate Key In The Same Table Doesn't Necessarily Result In A Composite Key.
                                            [*] The Alternate Key May Still Provide A Unique Constraint, But It Doesn't Have The Same Significance As The Primary Key.
                                        "
                                    );
                                }
                            }
                            _ => {
                                println!("Invalid Argument For Show Command");
                                println!("Usage: Show [inserts | keys | references | examples [optional - [add | del | (modify | mod) | (refs | references) | (attributes | attr)]]");
                            }
                        }
                    }
                    _ => {
                        println!("Invalid Command Entered");
                        println!("Type 'Help' For List of Commands");
                    }
                }
            }
        }
    }
}

pub(crate) fn display_help(display_all: bool) {
    /*
    Display the help menu.

    If `showAll` is False, only the default message will be displayed.
    If `showAll` is True, both the default message and the list of possible commands will be displayed.

    Args:
        show_all (bool): Whether to show all commands or not.


    This is a help message that displays on program startup and anytime the user inputs 'Help' command.
    The message provides instructions on how to use the program
    It includes details such as the acceptable format for defining SQL tables
    How to add tables to the program for generation
    How to view a list of all commands
    And how to clear the help message from the console.
    */

    println!(
        "
            ===== MESSAGE =====

            [*]This Program Takes User Input For SQL Table Values And Generates Random Insert Statements For Database Testing
            [*]All Tables Should Be Defined Using The Following Format:
                    Format: [Number Of Insert Statements][Table Name][Table Attributes]
            [*]An Example Of An Accepted Table Definition And An Accepted Add Command:
            [*]add 100 profile (PK userID INTEGER, name NAME, AK email EMAIL, password PASSWORD(30))
            [*]Where 'add' Adds The [Number Of Insert Statements][Table Name][Table Attributes] To A List Used For Generation
            [*]This Program Can Generate Any Number Of Inserts For Any Number Of SQL Tables As Long As User Memory Permits
            [*]The Commands For This Program Are Not Case Sensitive
            [*]Type 'Help' To Get A List Of All Commands And To Reprint This Message
            [*]To See Example Commands For Each Command:
                    Type 'show examples [specific command (optional - will show all examples if blank)]'
            [*]Type 'Clear' To Delete This Message
          \n"
    );

    if display_all {
        /*
        Prints The Commands And Definitions For The Program
        Showing The User All The Available Commands And Their Required Arguments.
        */
        println!(
            "
                        ===== COMMANDS AND DEFINITIONS=====

            Commands Followed By [] Are Required Args Unless Specifically Stated In This Menu

            Generate -> Begins Generating SQL Script Of Inserts [Mut Have At Least One Defined Table]
            Clear -> Clear Terminal Screen
            Help -> Show This Help Menu
            Exit | Quit -> Terminate Program

            Add  [numInserts][tableName][tableAttributes] -> Add Table To Generate Statements For, Where:
                 [numInserts] -> The Number Of Insert Statements To Generate For The Table
                 [tableName] -> The Name of The Table Being Created
                 [tableAttributes] -> All Defined Columns Of The Table [Format Shown Below]
                 [tableAttributes] Should Be Input In The Following Format:

                 [key definition][referenced_attr name][referenced_attr type][foreign table]

                 Where [key definition] and [foreign table] can be NULL
                 Key Definitions:
                    'PK' - Primary Key
                    'AK' - Alternate (Unique) Key
                    'FK' - Foreign Key -> [foreign table] - Table Being Referenced By The Attribute
                    'PK/FK' - Primary Key And Foreign Key -> [foreign table] - Table Being Referenced By The Attribute
                    'AK/FK' - Alt (Unique) And Foreign Key -> [foreign table] - Table Being Referenced By The Attribute

                 [Note 1]: In Cases Of An Attribute Being A Foreign Key, [foreign table] CANNOT be NULL
                 To Ensure A Foreign Key Attribute Is Properly Read And Referenced A Specific Format Is Provided:
                 [Foreign Table] Proper Format -> referenced_table(referenced_attribute)
                 For More Info On Referencing Syntax With Examples. Input 'show examples [refs | references]'

                 [Note 2]: In Tables With More Than One PK, It Will Generate Data As A Composite Pair
                 Hence The AK Attribute May Be Needed, This Enforces The Unique Values Without Worrying About Checking The Keys As Pairs

            Rm   [tableName] -> Remove Table From List
                 [tableName] Must Be The Same As It Was Defined In It's Add Statement

            Modify | Mod [tableName] [numStatements | tableName | referenced_attr] [newValue] -> Modify Existing Table
                 [tableName] -> The Table To Modify And Must Be The Same As It Was Defined In It's Add Statement
                 [numInserts | tableName | referenced_attr] -> The Definition Of The Tuple You Wish To Modify 
                 Note: You Can Only Modify One Definition At A Time
                 IMPORTANT REGARDING Modify [numInserts | tableName]:
                 If User Wishes To Modify The Number of Statements or Change Table Name. The User Should Input 'numStatements'
                 or 'tableName' in Place of First Arg After Specifying The Table: However, When Modifying [referenced_attr],
                 The User Should Input the Attribute Name They Wish To Change. Type 'show examples mod' For More Info
                 [*]For 'Modify [tableName] [referenced_attr]':
                    The User Must Redefine The Whole Attribute 
                    As In Redefine [key definition][referenced_attr name][referenced_attr type][foreign table]

            Show [inserts | keys | references | types | examples [Add | Del | (Modify | Mod) | (Refs | References) | (Attributes | Attr)]
                 [Inserts] -> Show's The Table's The Program Will Be Creating Insert Statements For
                 [Keys] -> Show's The List Of Keys For Each Table
                 [References | Refs] -> Show's All Referenced Attributes Between Two Tables In The Form Of:
                                        ([Referencing Table]:([Referenced Table]:[Referenced Attribute]))
                 [Types] -> Lists All Custom Defined Types And The Reasoning For Their Creation For This Program 
                 [Examples] -> Will Print Examples For Most Commands Taken By The Program
            "
        );
    }
}

pub(crate) fn set_variable_size(attr_type: &str) -> Option<u16> {
    /*
    Returns Either variable_size or None

    :param attr_type is the type being parsed
    :return some_returned_value: Option<u16>
    */
    let some_returned_value: Option<u16> = match &attr_type {
        s if s.contains("VARCHAR") => {
            let variable_size_str = &attr_type[7..];
            let variable_size_str: String =
                variable_size_str[1..variable_size_str.len() - 1].to_string();
            Some(variable_size_str.parse().unwrap())
        }
        s if s.contains("CHAR") => {
            let variable_size_str = &attr_type[4..];
            let variable_size_str = &variable_size_str[1..variable_size_str.len() - 1];
            Some(variable_size_str.parse().unwrap())
        }
        s if s.contains("PASSWORD") => {
            let variable_size_str = &attr_type[8..];
            let variable_size_str = &variable_size_str[1..variable_size_str.len() - 1];
            Some(variable_size_str.parse().unwrap())
        }
        _ => None,
    };
    some_returned_value
}

pub(crate) fn write_to_file(
    generated_statement: String,
    mut custom_path: Option<String>,
) -> io::Result<()> {
    /*
    Writes the given SQL statement to a file on disk.

    :param generated_statement: A string containing the SQL statement to write to file.
    :return: An `io::Result` indicating whether the write operation was successful.
    */

    if custom_path.is_none() {
        let user_folder = dirs::home_dir().unwrap();
        custom_path = Some(
            user_folder
                .join("Documents/sample-data.sql")
                .to_string_lossy()
                .to_string(),
        );
    }

    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(custom_path.unwrap())?;

    writeln!(file, "{}", generated_statement)?;

    Ok(())
}
