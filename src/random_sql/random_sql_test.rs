#[cfg(test)]
use crate::random_sql::random_sql::*;

#[test]
fn test_check_data_type_valid() {
    /*
    Test case for check_data_type() with a valid data type.

    Preconditions: random_sql is running and attributes are being loaded (sim)
    Execution steps: Call check_data_type() with the "VARCHAR" data type.
    Postconditions: The function returns true.
    Expected output: True.
    */

    let result = check_data_type("VARCHAR(50)");
    assert_eq!(result, true);
}

#[test]
fn test_check_data_type_invalid() {
    /*
    Test case for check_data_type() function when given an invalid data type.

    Preconditions:
        - random_sql is running and attributes are being loaded (sim)
    Execution steps:
        - Call check_data_type() function with an invalid data type, "invalid_type".
    Postconditions:
        - The function should return false.
    Expected output: The function should return false.
    */

    let attr_type = "invalid_type";
    let result = check_data_type(&attr_type);
    assert_eq!(result, false);
}

#[test]
fn test_check_data_type() {
    /*
     * Test case for check_data_type() with multiple valid and invalid data types.
     *
     * Preconditions: random_sql is running and parsing user input.
     * Execution steps:
     * 1. Pass in 5 valid data types and assert true on each.
     * 2. Pass in 3 invalid data types and assert false on each.
     * Postconditions: All assertions pass.
     */

    // Check valid data type
    assert!(check_data_type("INTEGER"));

    // Check invalid data type
    assert!(!check_data_type("FLO4"));

    // Check multiple valid data types
    assert!(check_data_type("BIGINT"));
    assert!(check_data_type("DATE"));
    assert!(check_data_type("DOUBLE PRECISION"));
    assert!(check_data_type("INTERVAL"));
    assert!(check_data_type("SMALLINT"));

    // Check multiple invalid data types
    assert!(!check_data_type("SMALLIN"));
    assert!(!check_data_type("DOUBL PRECISIO"));
    assert!(!check_data_type("INTRV"));
}

#[test]
fn test_check_key_definition_valid_pk() {
    /*
     * Test case for check_key_definition() function to check if the given key definition is valid.
     * Test scenario:
     *   - Preconditions: random_sql is running and parsing user input.
     *   - Input: "PK"
     *   - Expected Output: true
     *   - Postconditions: The function should return true.
     */
    assert_eq!(check_key_definition("PK"), true);
}

#[test]
fn test_check_key_definition_valid_pk_fk() {
    /*
     * Test case for check_key_definition() function to check if the given key definition is valid.
     * Test scenario:
     *   - Preconditions: random_sql is running and parsing user input.
     *   - Input: "PK/FK"
     *   - Expected Output: true
     *   - Postconditions: The function should return true.
     */

    // Define a valid PK/FK key definition
    let key_def = "PK/FK";

    // Check if the key definition is valid
    let result = check_key_definition(key_def);

    // Assert that the result is true
    assert_eq!(result, true);
}

#[test]
fn test_check_key_definition_valid_ak() {
    /*
     * Test case for check_key_definition() function to check if the given key definition is valid.
     * Test scenario:
     *   - Preconditions: random_sql is running and parsing user input.
     *   - Input: "AK"
     *   - Expected Output: true
     *   - Postconditions: The function should return true.
     */

    // Define a valid key definition
    let key_def = "AK";

    // Check if the key definition is valid
    let result = check_key_definition(key_def);

    // Assert that the result is true
    assert_eq!(result, true);
}

#[test]
fn test_check_key_definition_invalid_fk_pk() {
    /*
     * Test case for check_key_definition() function to check if the given key definition is invalid.
     * Test scenario:
     *   - Preconditions: random_sql is running and parsing user input.
     *   - Input: "FK/PK"
     *   - Expected Output: false
     *   - Postconditions: The function should return false.
     */

    // Define an ivalid key definition
    let key_def = "FK/PK";

    // Check if the key definition is valid
    let result = check_key_definition(key_def);

    // Assert that the result is true
    assert_eq!(result, false);
}

#[test]
fn test_check_key_definition_invalid_kf() {
    /*
     * Test case for check_key_definition() function to check if the given key definition is invalid.
     * Test scenario:
     *   - Preconditions: random_sql is running and parsing user input.
     *   - Input: "KF"
     *   - Expected Output: false
     *   - Postconditions: The function should return false.
     */

    // Define an ivalid key definition
    let key_def = "KF";

    // Check if the key definition is valid
    let result = check_key_definition(key_def);

    // Assert that the result is true
    assert_eq!(result, false);
}

